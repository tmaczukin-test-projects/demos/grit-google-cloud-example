terraform {
  required_version = ">= 1.7"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 5.12"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0"
    }
  }
}

locals {
  google_project_id = "tomasz-7dc9245e" # TODO: put your Google Cloud project ID here
  google_region     = "us-east1"
  google_zone       = "us-east1-b"

  gitlab_url = "https://gitlab.com"

  ###############################################################################################
  ###                    At least one of the below parameters must be set!                    ###

  gitlab_project_id = 0 # Use the existing project ID if you want to register runner through GRIT
  # instead of doing that manually through UI.

  runner_token = "" # Use the existing token!
  # If left empty, registration for the project configured
  # with gitlab_url and gitlab_project_id will happen

  ###############################################################################################

  metadata = {
    name   = "grit-gcp-test-1" # TODO: Update the name to make it different from previous test executions
    labels = {
      managed = "grit"
    }
    min_support = "experimental"
  }
}

provider "google" {
  project = local.google_project_id
  region  = local.google_region
  zone    = local.google_zone
}

module "vpc" {
  source = "git::https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git//modules/google/vpc/prod?ref=35de4a73b9edbea968d0e14fd267f6fd45d35d07"

  metadata = local.metadata

  google_region = local.google_region

  subnetworks = {
    runner-manager    = "10.0.0.0/24"
    ephemeral-runners = "10.1.0.0/22"
  }
}

module "iam" {
  source = "git::https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git//modules/google/iam/prod?ref=35de4a73b9edbea968d0e14fd267f6fd45d35d07"

  metadata = local.metadata
}

locals {
  register_runner_through_grit = local.runner_token == "" && local.google_project_id != 0
}

module "runner-registration" {
  count = register_runner_through_grit ? 1 : 0

  source = "git::https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git//modules/gitlab/prod?ref=35de4a73b9edbea968d0e14fd267f6fd45d35d07"

  metadata = local.metadata

  url                = local.gitlab_url
  project_id         = local.gitlab_project_id
  runner_description = "grit-gcp-demo-runner"
  runner_tags        = ["grit-runner"]
}

module "runner" {
  source = "git::https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git//modules/google/runner/prod?ref=35de4a73b9edbea968d0e14fd267f6fd45d35d07"

  metadata = local.metadata

  google_project = local.google_project_id
  google_zone    = local.google_zone

  service_account_email = module.iam.service_account_email

  vpc = {
    id        = module.vpc.id
    subnet_id = module.vpc.subnetwork_ids["runner-manager"]
  }

  gitlab_url   = local.gitlab_url
  runner_token = register_runner_through_grit ? module.runner-registration.runner_token : local.runner_token

  executor = "docker-autoscaler"

  cache_gcs_bucket = module.cache.bucket_name

  fleeting_instance_group_name = module.fleeting.instance_group_name

  runners_global_section = <<EOS
  environment = [
    "DOCKER_TLS_CERTDIR=",
    "DOCKER_DRIVER=overlay2"
  ]

  [runners.feature_flags]
    FF_USE_IMPROVED_URL_MASKING = true
    FF_RESOLVE_FULL_TLS_CHAIN = false
EOS

  runners_docker_section = <<EOS
    volumes = [
      "/certs/client"
    ]

    privileged = true
EOS
}

module "fleeting" {
  source = "git::https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git//modules/google/fleeting/prod?ref=35de4a73b9edbea968d0e14fd267f6fd45d35d07"

  metadata = local.metadata

  google_project = local.google_project_id

  fleeting_service = "gce"

  service_account_email = module.iam.service_account_email

  vpc = {
    id        = module.vpc.id
    subnet_id = module.vpc.subnetwork_ids["ephemeral-runners"]
  }
  manager_subnet_cidr = module.vpc.subnetwork_cidrs["runner-manager"]
}

module "cache" {
  source = "git::https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git//modules/google/cache/prod?ref=35de4a73b9edbea968d0e14fd267f6fd45d35d07"

  metadata = local.metadata

  bucket_location = local.google_region

  service_account_emails = [
    module.iam.service_account_email
  ]
}

output "runner_manager_external_ip" {
  value = module.runner.external_ip
}
