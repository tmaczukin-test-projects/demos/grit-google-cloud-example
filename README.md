# GRIT on Google Cloud - DEMO

Demo of GRIT execution on Google Cloud.

[TOC]

## Features covered in the demo setup

In this demo we will cover following features:

1. Deploying new, complete infrastructure

    It's possible with GRIT to use some existing resources like for example
    a VPC, or subnets, or GCS buckets etc. We will not use that in this demo.

    In this demo we will create all needed infrastructure from scratch, using
    GRIT submodules.

1. Deploying a runner manager with a pre-created token.

    Registration of new runner through GRIT is available option, but
    we will not cover it here.

1. Autoscaling for Linux with Docker support

    Runner will be installed on a Linux instance in an autoscaling mode.
    Jobs will be executed in containers.

1. Remote cache

    We will create GCS bucket for cache and configure runner to support remote cache.

## Prerequisites

1. GitLab project and maintainer access to it

    This is where we will add the runner and execute a test pipeline.

1. Google Cloud project and sufficient access to it

    For the demo purpose terraform execution is done using my personal account
    logged in console with `gcloud`. As I'm the owner of the project I have all
    required permissions. List of permissions needed to run GRIT template for
    Google Cloud will be established in a later iteration.

1. Three Google Cloud APIs need to be enabled in the Google Cloud project:

    - Compute Engine API - this is needed for GRIT to run and for runner execution
      (in autoscaling mode, which is the case for this demo)
    - Cloud KMS API - this is needed for GRIT to run and runner deployment to
      be able to decrypt secrets
    - IAM Service Account Credentials API - this is needed for runner execution
      to be able to pre-sign GCS URLs (needed for remote cache feature)

## Create new runner through UI

Go to the test project and create a new runner in it:

https://gitlab.com/tmaczukin-test-projects/grit-gce-tests/-/runners/new

Copy the token for further usage.

Open the runner details page - we will later use it to check if it connected
properly after terraform execution.

BTW this is the place where we plan to print the terraform code for the user
when Google Cloud integration is detected.

## Clone and adjust terraform configuration

Clone test configuration (this project) locally:

```shell
cd /tmp
git clone https://gitlab.com/tmaczukin-test-projects/demos/grit-google-cloud-example.git
cd grit-google-cloud-example
vim main.tf
```

Open the `main.tf` file, look for the `locals` section and update the configuration
details with the token of the new runner from the previous step. It's also worth to
set a "fresh" name in the metadata variable.

> Why? Because we create custom roles in Google Cloud IAM. And custom role, after
> it is removed, stays in the project for next 30 days. During these 30 days we can't
> re-use the name for a new role.
> As this Google Cloud project was used for experimentation, it will have a number of
> previously created roles that are already scheduled for deletion. Having a "fresh"
> name will make sure that terraform execution will work.

## Run terraform code

Go back to the terminal and execute terraform code:

```shell
terraform init
terraform plan -out plan.tfcache
time terraform apply plan.tfcache
```

## Execute test pipeline

Go back to the test project and check the registered runner - it should be marked
as active already and the details like IP address and version should be already
filed in the details preview.

If that is done, start a new pipeline to see how jobs are executed on the runner:
https://gitlab.com/tmaczukin-test-projects/grit-gce-tests/-/pipelines/new

## Review created resources

- [IAM](https://console.cloud.google.com/iam-admin/iam?project=tomasz-7dc9245e) - we should see the service account to be
  granted with the two custom roles.
- [Service accounts](https://console.cloud.google.com/iam-admin/serviceaccounts?project=tomasz-7dc9245e) - we should see the
  service account for runner manager.
- [Roles](https://console.cloud.google.com/iam-admin/roles?project=tomasz-7dc9245e) - we should see the two new custom roles.
- [VM instances](https://console.cloud.google.com/compute/instances?project=tomasz-7dc9245e) - we should see runner manager
  and at least few ephemeral runner VMs.
- [Instance templates](https://console.cloud.google.com/compute/instanceTemplates/list?project=tomasz-7dc9245e) - we should
  see the instance template used by instance group when autoscaling.
- [Instance groups](https://console.cloud.google.com/compute/instanceGroups/list?project=tomasz-7dc9245e) - we should see the
  new instance group.
- [Buckets](https://console.cloud.google.com/storage/browser?project=tomasz-7dc9245e) - we should see the bucket for cache.
  Entering it, in permissions we should see the service account to be granted with `objectAdmin` role.
- [Key management](https://console.cloud.google.com/security/kms/keyrings?project=tomasz-7dc9245e) - we should see the new
  keyring.
- [VPC networks](https://console.cloud.google.com/networking/networks/list?project=tomasz-7dc9245e) - we should see the new VPC.
  entering it we should see the two subnets we've requested. We can also see the firewall rules to be
  added.

## Review runner configuration and logs

Login to the runner instance using os-login. IP address of runner manager is printed as one of
terraform outputs.

> OS Login requires some preparation. You need to add your public SSH key and connect it with your Google Cloud
> account. This is described at https://cloud.google.com/sdk/gcloud/reference/compute/os-login/ssh-keys/add.
> Having that SSH works by using a username formatted as `username_your_domain_com`. For example, if you're
> logged into Google Cloud as `user@example.com`, your OS Login username will be `user_example_com`.
> Local configuration of SSH to work with keypair is the obvious I'll me not discussing here :)

After logging to the instance we can review the configuration file:

```shell
sudo cat /etc/gitlab-runner/config.toml
```

We can also see the logs:

```shell
sudo docker logs -n 1000 -f gitlab-runner
```

## Cleanup (optional)

Remove the created runner.

Remove Google Cloud resources through terraform:

```shell
time terraform destroy
```
